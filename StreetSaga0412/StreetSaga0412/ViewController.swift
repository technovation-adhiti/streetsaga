//
//  ViewController.swift
//  StreetSaga0412
//
//  Created by Girl Scout Dev on 4/12/17.
//  Copyright © 2017 Girl Scout Dev. All rights reserved.
//

import UIKit


class ViewController: UIViewController {

    // adding background pic container    - Add logo here ( Anikas Webpage)
    let imageView = UIImageView.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        view.backgroundColor = UIColor.black
        // add image to background pic container
        imageView.image = UIImage(named: "cropped-programs-03-Homeless-Outreach.jpg")
        imageView.autoresizingMask = [.flexibleWidth , .flexibleHeight]
        imageView.contentMode = .scaleAspectFill
        imageView.frame = view.bounds
        imageView.alpha = 0
        view.addSubview(imageView)
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear (animated)
        
        UIView.animate(withDuration: 0.5)
        {
            self.imageView.alpha = 1
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now()){
           self.showOptions()}
    }
    
    
    //this function describes how the background change works
    func changeBackground(imageName : String)
    {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: "easeInEaseOut")
        transition.type = "fade"
        imageView.layer.add(transition, forKey: nil)
        imageView.image = UIImage(named: imageName)
    }
    
    //this function creates the first set of buttons
    func showOptions()
    {
        changeBackground(imageName: "smiling.png")
    
        PCRapidSelectionView.view(forParentView: self.view, theme: IPDFUIThemeDark, currentGuestureRecognizer: nil, interactive: false, options: ["I would like help","I want to help", "Stories & Photos" ], title: "") {(choice : Int) in
            if (choice == NSNotFound)
            {
                self.showOptions()
            }
            if (choice == 0)
            {
                self.wantHelp()
            }
            if (choice == 1)
            {
                self.giveHelp()
            }
            if (choice == 2)
            {
                self.readStories()
            }
        }
    }

    //this function creates the I want help buttons
    func wantHelp()
    {
        changeBackground(imageName: "background-test.jpg")
        
        PCRapidSelectionView.view(forParentView: self.view, theme: IPDFUIThemeDark, currentGuestureRecognizer: nil, interactive: false, options: ["I would like food","I would like clothing", "I would like supplies", "I would like to find shelter" ], title: "What do you need?") {(choice : Int) in
            if (choice == NSNotFound)
            {
                self.showOptions()
            }
            if (choice == 0)
            {
                self.likeFood()
            }
            if (choice == 1)
            {
                self.likeClothing()
            }
            if (choice == 2)
            {
               self.likeSupplies()
            }
            if (choice == 3)
            {
                self.likeShelter()
            }
        
        }
    }

    //this function creates the the give help buttons
    func giveHelp()
    {
        changeBackground(imageName: "kid.jpeg")
        
        PCRapidSelectionView.view(forParentView: self.view, theme: IPDFUIThemeDark, currentGuestureRecognizer: nil, interactive: false, options: ["I want to give clothes","I want to give food", "I want to volunteer" ], title: "What would you like to donate?") {(choice : Int) in
            if (choice == NSNotFound)
            {
                self.showOptions()
            }
            if (choice == 0)
            {
                self.giveClothing()
            }
            if (choice == 1)
            {
                self.giveFood()
            }
            if (choice == 2)
            {
                self.giveHours()
            }
            
        }
    }
    
    //this function creates website buttons for homeless
    func likeFood()
    {
        changeBackground(imageName: "homeless.jpeg")
       
        PCWebView.view(forParentView: self.view, title: "Services", url: URL(string: "http://silversaga.designcodebuild.com/i-would-like-food/"), shouldDismissHandler: { (urlString : String?) -> Bool in if (urlString == "sos://doSomething")
        {
            self.wantHelp()
            return true
            }
            return false
        }, dismissHandler:
            {
                self.wantHelp()
            })
        
        
    }
    
    func likeClothing()
    {
        changeBackground(imageName: "homeless.jpeg")
        
        PCWebView.view(forParentView: self.view, title: "Services", url: URL(string: "http://silversaga.designcodebuild.com/i-would-like-clothing/"), shouldDismissHandler: { (urlString : String?) -> Bool in if (urlString == "sos://doSomething")
        {
            self.wantHelp()
            return true
            }
            return false
        }, dismissHandler:
            {
                self.wantHelp()
        })
    }
    
    func likeSupplies()
    {
        changeBackground(imageName: "homeless.jpeg")
            
        PCWebView.view(forParentView: self.view, title: "Services", url: URL(string: "http://silversaga.designcodebuild.com/i-would-like-supplies/"), shouldDismissHandler: { (urlString : String?) -> Bool in if (urlString == "sos://doSomething")
        {
            self.wantHelp()
            return true
            }
            return false
        }, dismissHandler:
            {
                self.wantHelp()
        })
    }
   
    func likeShelter()
    {
        changeBackground(imageName: "homeless.jpeg")
        
        PCWebView.view(forParentView: self.view, title: "Services", url: URL(string: "http://silversaga.designcodebuild.com/i-would-like-to-find-shelter/"), shouldDismissHandler: { (urlString : String?) -> Bool in if (urlString == "sos://doSomething")
        {
            self.wantHelp()
            return true
            }
            return false
        }, dismissHandler:
            {
                self.wantHelp()
        })
    }

    
    
    // Connect to za Facebook stories

func readStories()
{
    changeBackground(imageName: "homeless.jpeg")
    
    PCWebView.view(forParentView: self.view, title: "Services", url: URL(string: "https://silversaga.designcodebuild.com/homeless-humans-of-san-diego/"), shouldDismissHandler: { (urlString : String?) -> Bool in if (urlString == "sos://doSomething")
    {
        self.showOptions()
        return true
        }
        return false
    }, dismissHandler:
        {
            self.showOptions()
    })
    
    
}
    
    // I want to help buttons
    
    func giveClothing()
    {
        changeBackground(imageName: "homeless.jpeg")
        
        PCWebView.view(forParentView: self.view, title: "Services", url: URL(string: "http://silversaga.designcodebuild.com/calendar-for-clothing/"), shouldDismissHandler: { (urlString : String?) -> Bool in if (urlString == "sos://doSomething")
        {
            self.giveHelp()
            return true
            }
            return false
        }, dismissHandler:
            {
                self.giveHelp()
        })
    }
    
    func giveFood()
    {
        changeBackground(imageName: "homeless.jpeg")
        
        PCWebView.view(forParentView: self.view, title: "Services", url: URL(string: "http://silversaga.designcodebuild.com/calendar-for-food/"), shouldDismissHandler: { (urlString : String?) -> Bool in if (urlString == "sos://doSomething")
        {
            self.giveHelp()
            return true
            }
            return false
        }, dismissHandler:
            {
                self.giveHelp()
        })
    }
    
    func giveHours()
    {
        changeBackground(imageName: "homeless.jpeg")
        
        PCWebView.view(forParentView: self.view, title: "Services", url: URL(string: "http://silversaga.designcodebuild.com/calendar-for-volunteering/"), shouldDismissHandler: { (urlString : String?) -> Bool in if (urlString == "sos://doSomething")
        {
            self.giveHelp()
            return true
            }
            return false
        }, dismissHandler:
            {
                self.giveHelp()
        })
    }
    
    }
    
    
    
    
    
    



