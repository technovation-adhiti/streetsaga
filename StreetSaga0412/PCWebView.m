//
//  PCWebView.m
//  Silver Saga
//
//  Created by Maximilian Mackh on 25/03/2017.
//  Copyright © 2017 touchgrove llc. All rights reserved.
//

#import "PCWebView.h"

#import "PCSplitView.h"

@interface PCWebView () <UIWebViewDelegate>

@property (nonatomic,weak) UIView *parentView;

@property (nonatomic,copy) BOOL (^shouldDismissHandler)(NSString *link);
@property (nonatomic,copy) void (^dismissHandler)();

@end

@implementation PCWebView

+ (void)viewForParentView:(UIView *)parentView title:(NSString *)title url:(NSURL *)url shouldDismissHandler:(BOOL(^)(NSString *link))shouldDismissHandler dismissHandler:(void(^)())dismissHandler;
{
    PCWebView *webView = [PCWebView new];
    webView.shouldDismissHandler = shouldDismissHandler;
    webView.dismissHandler = dismissHandler;
    webView.parentView = parentView;
    [webView showWithTitle:title URL:url];
}

- (void)showWithTitle:(NSString *)title URL:(NSURL *)url
{
    UIVisualEffectView *blurView = [[UIVisualEffectView alloc] initWithEffect:[UIBlurEffect effectWithStyle:UIBlurEffectStyleDark]];
    blurView.frame = self.bounds;
    blurView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self addSubview:blurView];
    
    
    __weak typeof(self) weakSelf = self;
    
    PCSplitView *containerSplitView = [PCSplitView splitViewWithSubviewLayout:@"0*30,1*-1,0*10,0*20,0*10" direction:PCSplitViewDirectionVertical configurationHandler:^(PCSplitView *containerSplitView)
    {
        [containerSplitView addSubviewWithColor:nil];
        
        PCSplitView *splitView = [PCSplitView splitViewWithSubviewLayout:@"0*30,0*1,1*-1" direction:PCSplitViewDirectionVertical configurationHandler:^(PCSplitView *splitView)
        {
            UILabel *titleLabel = [UILabel new];
            titleLabel.textColor = [UIColor colorWithWhite:0.5 alpha:1];
            titleLabel.text = title;
            titleLabel.font = [UIFont systemFontOfSize:13];
            titleLabel.textAlignment = NSTextAlignmentCenter;
            [splitView addSubview:titleLabel];
            
            [splitView addSubviewWithColor:nil];
            
            UIWebView *webView = [UIWebView new];
            [webView loadRequest:[NSURLRequest requestWithURL:url]];
            webView.opaque = NO;
            webView.backgroundColor = [UIColor whiteColor];
            webView.delegate = weakSelf;
            webView.scalesPageToFit = YES;
            [splitView addSubview:webView];
            
        }];
        splitView.layer.backgroundColor = [UIColor blackColor].CGColor;
        splitView.layer.cornerRadius = 10;
        splitView.clipsToBounds = YES;
        [containerSplitView addSubview:splitView];
        
        [containerSplitView addSubviewWithColor:nil];
        
        UILabel *dismiss = [UILabel new];
        dismiss.text = @"Dismiss";
        dismiss.textAlignment = NSTextAlignmentCenter;
        dismiss.textColor = [UIColor colorWithWhite:1 alpha:0.7];
        dismiss.font = [UIFont systemFontOfSize:14];
        [containerSplitView addSubview:dismiss];
        
        [containerSplitView addSubviewWithColor:nil];
    }];
    [self addSubview:containerSplitView];
    [containerSplitView snapToSuperview];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismiss)];
    [self addGestureRecognizer:tap];
    
    self.alpha = 0;
    
    self.frame = self.parentView.bounds;
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.parentView addSubview:self];
    
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^
    {
        self.alpha = 1;
    } completion:nil];
}

- (void)dismiss
{
    [UIView animateWithDuration:0.5 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^
    {
         self.alpha = 0;
    }
    completion:^(BOOL finished)
    {
        [self removeFromSuperview];
        if (self.dismissHandler) self.dismissHandler();
    }];
    
}

#pragma mark -
#pragma mark Web View Delegate

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked)
    {
        NSString *link = request.URL.absoluteString;
        if (self.shouldDismissHandler && self.shouldDismissHandler(link))
        {
            [self dismiss];
            return NO;
        }
    }
    return YES;
}

@end
