# README #

### The iOS app code for the Technovation Challenge can be found [here](https://bitbucket.org/technovation-adhiti/streetsaga/downloads/).  ###

### The link to the zip file of the Street Saga website can be found [here](http://pluginapi32.blogvault.net/996018716_silversaga.designcodebuild.com.zip). ###

### Who do I talk to? ###

* Contact gstechnovation@gmail.com or the owner of this repo with questions. Thank you!