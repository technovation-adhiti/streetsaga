//
//  PCWebView.h
//  Silver Saga
//
//  Created by Maximilian Mackh on 25/03/2017.
//  Copyright © 2017 touchgrove llc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PCWebView : UIView

+ (void)viewForParentView:(UIView *)parentView title:(NSString *)title url:(NSURL *)url shouldDismissHandler:(BOOL(^)(NSString *link))shouldDismissHandler dismissHandler:(void(^)())dismissHandler;

@end
